<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://club.wpeka.com
 * @since             1.0.0
 * @package           Analytics_Api
 *
 * @wordpress-plugin
 * Plugin Name:       Analytics API
 * Plugin URI:        https://club.wpeka.com/
 * Description:       Analytics API plugin. Deployed on API server for usage tracking and plugin uninstall feedback.
 * Version:           1.0.0
 * Author:            WPEka Club
 * Author URI:        https://club.wpeka.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       analytics-api
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( file_exists( plugin_dir_path( __FILE__ ) . 'vendor/autoload.php' ) ) {
	require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'ANALYTICS_API_VERSION', '1.0.0' );

if ( ! defined( 'ANALYTICS_API_MIXPANEL_TRACKING_ID' ) ) {
	define( 'ANALYTICS_API_MIXPANEL_TRACKING_ID', '6fe1f96799f26a2ffde033b08ef9b263' );
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-analytics-api-activator.php
 */
function activate_analytics_api() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-analytics-api-activator.php';
	Analytics_Api_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-analytics-api-deactivator.php
 */
function deactivate_analytics_api() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-analytics-api-deactivator.php';
	Analytics_Api_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_analytics_api' );
register_deactivation_hook( __FILE__, 'deactivate_analytics_api' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-analytics-api.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_analytics_api() {

	$plugin = new Analytics_Api();
	$plugin->run();

}
run_analytics_api();

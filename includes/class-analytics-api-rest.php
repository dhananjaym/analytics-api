<?php
/**
 * The rest-specific functionality of the plugin.
 *
 * @link       https://club.wpeka.com
 * @since      1.0.0
 *
 * @package    Analytics_Api
 * @subpackage Analytics_Api/includes
 */

/**
 * The rest-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the rest-specific stylesheet and JavaScript.
 *
 * @package    Analytics_Api
 * @subpackage Analytics_Api/includes
 * @author     WPEka Club <support@wpeka.com>
 */
class Analytics_Api_Rest {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register API routes.
	 *
	 * @return bool
	 */
	public function analytics_api_rest_init() {
		if ( ! function_exists( 'register_rest_route' ) ) {
			return false;
		}
		register_rest_route(
			'action/submit/uninstall/',
			'reason',
			array(
				'methods'             => 'POST',
				'callback'            => array( $this, 'analytics_api_deactivation_feedback' ),
				'permission_callback' => array( $this, 'analytics_api_permissions_check' ),
			)
		);
		register_rest_route(
			'uninstall/reason/',
			'theme',
			array(
				'methods'             => 'GET',
				'callback'            => array( $this, 'analytics_api_get_uninstall_reasons' ),
				'permission_callback' => array( $this, 'analytics_api_permissions_check' ),
			)
		);
		register_rest_route(
			'action/submit/usage/',
			'stats',
			array(
				'methods'             => 'POST',
				'callback'            => array( $this, 'analytics_api_usage_stats' ),
				'permission_callback' => array( $this, 'analytics_api_permissions_check' ),
			)
		);
		register_rest_route(
			'action/track/analytics',
			'click',
			array(
				'methods'             => 'POST',
				'callback'            => array( $this, 'analytics_api_track_analytics' ),
				'permission_callback' => array( $this, 'analytics_api_permissions_check' ),
			)
		);
	}

	/**
	 * Return uninstall reasons.
	 *
	 * @param WP_REST_Request $request Request parameter.
	 * @return array
	 */
	public function analytics_api_get_uninstall_reasons( WP_REST_Request $request ) {
		$module_type          = $request->get_param( 'module_type' );
		$deactivation_reasons = array(
			array(
				'id'                => 1,
				'text'              => "I couldn't understand how to make it work",
				'input_type'        => '',
				'input_placeholder' => '',
			),
			array(
				'id'                => 2,
				'text'              => sprintf( "The %s is great, but I need specific feature that you don't support", $module_type ),
				'input_type'        => 'textarea',
				'input_placeholder' => 'What feature?',
			),
			array(
				'id'                => 3,
				'text'              => sprintf( 'The %s is not working', $module_type ),
				'input_type'        => 'textarea',
				'input_placeholder' => "Kindly share what didn't work so we can fix it for future users...",
			),
			array(
				'id'                => 4,
				'text'              => "It's not what I was looking for",
				'input_type'        => 'textarea',
				'input_placeholder' => "What you've been looking for?",
			),
			array(
				'id'                => 5,
				'text'              => sprintf( "The %s didn't work as expected", $module_type ),
				'input_type'        => 'textarea',
				'input_placeholder' => 'What did you expect?',
			),
			array(
				'id'                => 6,
				'text'              => sprintf( 'I found a better %s', $module_type ),
				'input_type'        => 'textfield',
				'input_placeholder' => sprintf( "What's the %s's name?", $module_type ),
			),
			array(
				'id'                => 7,
				'text'              => sprintf( "It's a temporary %s switch. I'm just debugging an issue.", $module_type ),
				'input_type'        => '',
				'input_placeholder' => '',
			),
			array(
				'id'                => 8,
				'text'              => 'Other',
				'input_type'        => 'textfield',
				'input_placeholder' => '',
			),
		);
		return $deactivation_reasons;
	}

	/**
	 * Record deactivate feedback into mixpanel.
	 *
	 * @param WP_REST_Request $request Request parameter.
	 * @return WP_Error|WP_REST_Response
	 */
	public function analytics_api_deactivation_feedback( WP_REST_Request $request ) {
		$deactivation_reason = $request->get_param( 'deactivation_reason' );
		$mp                  = Mixpanel::getInstance( ANALYTICS_API_MIXPANEL_TRACKING_ID );
		$product_name        = $request->get_param( 'product_name' );
		// Track an event.
		$mp->track(
			'Uninstalled ' . $product_name,
			array(
				'reason'           => $deactivation_reason,
				'reason_info'      => $request->get_param( 'reason_info' ),
				'platform_version' => $request->get_param( 'platform_version' ),
				'user_nickname'    => $request->get_param( 'user_nickname' ),
				'user_email'       => $request->get_param( 'user_email' ),
				'slug'             => $request->get_param( 'slug' ),
				'version'          => $request->get_param( 'version' ),
				'site_url'         => $request->get_param( 'site_url' ),
			)
		);
		$response_args = array( 'success' => true );
		return rest_ensure_response( $response_args );
	}

	/**
	 * Record usage stats into mixpanel.
	 *
	 * @param WP_REST_Request $request Request parameter.
	 * @return WP_Error|WP_REST_Response
	 */
	public function analytics_api_usage_stats( WP_REST_Request $request ) {
		$product_name = $request->get_param( 'product_name' );
		$default_data = json_decode( $request->get_param( 'default' ) );
		$server_data  = json_decode( $request->get_param( 'server' ) );
		$plugins_data = json_decode( $request->get_param( 'plugins' ) );
		$themes_data  = json_decode( $request->get_param( 'themes' ) );

		$mixpanel_data                   = array();
		$heading                         = 'Usage Analytics data for ' . $product_name;
		$mp                              = Mixpanel::getInstance( ANALYTICS_API_MIXPANEL_TRACKING_ID );
		$mixpanel_data['usage_data_for'] = $product_name;
		if ( isset( $default_data ) && ! empty( $default_data ) ) {
			$data = (array) $default_data;
			foreach ( $data as $key => $value ) {
				$mixpanel_data[ $key ] = $value;
			}
		}
		if ( isset( $server_data ) && ! empty( $server_data ) ) {
			$data = (array) $server_data;
			foreach ( $data as $key => $value ) {
				if ( 'curl_version' === $key || 'php_extensions' === $key ) {
					if ( 'curl_version' === $key ) {
						$mixpanel_data['curl_version']     = isset( $value->version ) ? $value->version : '';
						$mixpanel_data['curl_ssl_support'] = isset( $value->ssl_support ) ? $value->ssl_support : '';
					}
					if ( 'php_extensions' === $key ) {
						$php_extensions = '';
						$value          = (array) $value;
						foreach ( $value as $k => $v ) {
							if ( isset( $value[ $k ] ) && ! empty( $value[ $k ] ) ) {
								$php_extensions .= $k . ', ';
							}
						}
						if ( ! empty( $php_extensions ) ) {
							$php_extensions = rtrim( $php_extensions, ', ' );
						}
						$mixpanel_data['php_extensions'] = $php_extensions;
					}
				} else {
					$mixpanel_data[ $key ] = $value;
				}
			}
		}
		if ( isset( $plugins_data ) && ! empty( $plugins_data ) ) {
			$data    = (array) $plugins_data;
			$plugins = '';
			foreach ( $data as $key => $value ) {
				$plugins .= isset( $value->name ) ? isset( $value->version ) ? $value->name . ' (Ver.' . $value->version . ')' . ', ' : $value->name . ', ' : '';
			}
			if ( ! empty( $plugins ) ) {
				$plugins = rtrim( $plugins, ', ' );
			}
			$mixpanel_data['plugins'] = $plugins;
		}
		if ( isset( $themes_data ) && ! empty( $themes_data ) ) {
			$data = (array) $themes_data;
			foreach ( $data as $key => $value ) {
				if ( 'author' === $key ) {
					$mixpanel_data['theme_author_name'] = isset( $value->name ) ? $value->name : '';
					$mixpanel_data['theme_author_url']  = isset( $value->url ) ? $value->url : '';
				} else {
					$mixpanel_data[ 'theme_' . $key ] = $value;
				}
			}
		}

		// Track an event.
		$mp->track( $heading, $mixpanel_data );

		$response_args = array( 'success' => true );
		return rest_ensure_response( $response_args );
	}

	/**
	 * Record click tracks into mixpanel.
	 *
	 * @param WP_REST_Request $request Request parameter.
	 * @return WP_Error|WP_REST_Response
	 */
	public function analytics_api_track_analytics( WP_REST_Request $request ) {
		$product_name           = $request->get_param( 'product_name' );
		$event                  = $request->get_param( 'event' );
		$default_data           = json_decode( $request->get_param( 'default' ) );
		$mixpanel_data          = array();
		$heading                = 'Analytics Tracking data for ' . $product_name;
		$mp                     = Mixpanel::getInstance( ANALYTICS_API_MIXPANEL_TRACKING_ID );
		$mixpanel_data['event'] = $event;
		if ( isset( $default_data ) && ! empty( $default_data ) ) {
			$data = (array) $default_data;
			foreach ( $data as $key => $value ) {
				$mixpanel_data[ $key ] = $value;
			}
		}
		// Track an event.
		$mp->track( $heading, $mixpanel_data );
		$response_args = array( 'success' => true );
		return rest_ensure_response( $response_args );
	}

	/**
	 * Check API permissions.
	 *
	 * @param Object $request Request object.
	 * @return bool
	 */
	public function analytics_api_permissions_check( $request ) {
		return true;
	}

}
